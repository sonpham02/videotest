import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text, TouchableOpacity
} from 'react-native';

import ImagePicker from 'react-native-image-picker';

const options = {
    title: 'Select Video',
    mediaType: 'video',
    storageOptions: {
        skipBackup: true,
        // path: 'images',
    },
};

export default class UploadVideo extends Component {
    showPicker = () => {
        /**
         * The first arg is the options object for customization (it can also be null or omitted for default options),
         * The second arg is the callback which sends object: response (more info in the API Reference)
         */
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };

                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
            }
        });
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={{flex: 0, flexDirection: 'row', justifyContent: 'center'}}>
                    <TouchableOpacity onPress={() => this.showPicker()} style={styles.capture}>
                        <Text style={{fontSize: 14}}> Upload </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {

    },

    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },
});
