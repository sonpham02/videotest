import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text, TouchableOpacity
} from 'react-native';
import {RNCamera} from "react-native-camera";

export default class CameraView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            recording: false
        }
    }

    startRecording = async() => {
        this.setState({ recording: true });
        // default to mp4 for android as codec is not set
        const { uri, codec = "mp4" } = await this.camera.recordAsync();

        this.setState({ recording: false });
        console.log(uri);
    };

    stopRecording = () => {
        this.camera.stopRecording();
    };

    render() {
        const { recording} = this.state;

        return (
            <View style={styles.container}>
                <RNCamera
                    ref={ref => {
                        this.camera = ref;
                    }}
                    style={styles.preview}
                    type={RNCamera.Constants.Type.back}
                    flashMode={RNCamera.Constants.FlashMode.on}
                    androidCameraPermissionOptions={{
                        title: 'Permission to use camera',
                        message: 'We need your permission to use your camera',
                        buttonPositive: 'Ok',
                        buttonNegative: 'Cancel',
                    }}
                    androidRecordAudioPermissionOptions={{
                        title: 'Permission to use audio recording',
                        message: 'We need your permission to use your audio',
                        buttonPositive: 'Ok',
                        buttonNegative: 'Cancel',
                    }}
                    // onGoogleVisionBarcodesDetected={({barcodes}) => {
                    //     console.log(barcodes);
                    // }}
                />
                <View style={{flex: 0, flexDirection: 'row', justifyContent: 'center'}}>
                    <TouchableOpacity onPress={() => recording ? this.stopRecording() : this.startRecording()} style={styles.capture}>
                        <Text style={{fontSize: 14}}> {recording ? `Recording` : `Start`} </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
